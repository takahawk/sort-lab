package org.bitbucket.takahawk.sortlab.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * Created by takahawk on 19.03.16.
 */
public class ArrayGenerator {
    Random rand = new Random();

    public void toFile(File file, int n) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(file);
        pw.println(n);
        for (int i = 0; i < n; i++) {
            pw.println(rand.nextInt(10000));
        }
        pw.flush();
        pw.close();
    }
}
