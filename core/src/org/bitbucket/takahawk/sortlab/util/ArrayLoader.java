package org.bitbucket.takahawk.sortlab.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by takahawk on 19.03.16.
 */
public class ArrayLoader {

    public int[] fromFile(File file) throws FileNotFoundException {
        Scanner in = new Scanner(file);
        int[] result = new int[Integer.parseInt(in.next())];
        for (int i = 0; i < result.length; i++) {
            result[i] = Integer.parseInt(in.next());
        }
        return result;
    }
}
