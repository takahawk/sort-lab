package org.bitbucket.takahawk.sortlab.scenes;

import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by takahawk on 19.03.16.
 */
public interface Scene {
    Stage getStage();
    void render(float dt);
}
