package org.bitbucket.takahawk.sortlab.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kotcrab.vis.ui.widget.VisTable;
import net.dermetfan.gdx.scenes.scene2d.ui.FileChooser;
import net.dermetfan.gdx.scenes.scene2d.ui.TreeFileChooser;
import org.bitbucket.takahawk.sortlab.SortLab;

import java.io.File;

/**
 * Created by takahawk on 19.03.16.
 */
public class FileScene implements Scene {
    private Stage stage;
    private SortLab context;
    private AssetManager assetManager;
    private Skin uiSkin;
    private FileHandle lastFile = null;
    public FileScene(Viewport viewport, SortLab context) {
        this.context = context;
        stage = new Stage(viewport);
        assetManager = context.getAssetManager();
        assetManager.load("skins/uiskin/uiskin.json", Skin.class);
        assetManager.finishLoading();
        uiSkin = assetManager.get("skins/uiskin/uiskin.json");
        initStage();
    }

    public void initStage() {
        TreeFileChooser fileChooser = new TreeFileChooser(uiSkin, new FileChooser.Listener() {
            @Override
            public void choose(FileHandle file) {
                lastFile = file;
                context.setMainScene();
            }

            @Override
            public void choose(Array<FileHandle> files) {
                lastFile = files.get(0);
                context.setMainScene();
            }

            @Override
            public void cancel() {
                context.setMainScene();
            }
        });

        for (File f : File.listRoots()) {
            fileChooser.add(new FileHandle(f));
        }
        fileChooser.setFillParent(true);
        fileChooser.getCell(fileChooser.getTreePane()).expand().align(Align.topLeft).pad(20);
        stage.addActor(fileChooser);
    }

    public FileHandle getLastFile() {
        return lastFile;
    }

    @Override
    public Stage getStage() {
        return stage;
    }

    @Override
    public void render(float dt) {
        stage.act(dt);
        stage.draw();
    }
}
