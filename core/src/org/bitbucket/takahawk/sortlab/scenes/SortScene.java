package org.bitbucket.takahawk.sortlab.scenes;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.util.Validators;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import org.bitbucket.takahawk.sortlab.SortLab;
import org.bitbucket.takahawk.sortlab.actors.SortActor;
import org.bitbucket.takahawk.sortlab.sort.BubbleSorter;
import org.bitbucket.takahawk.sortlab.sort.SortTask;
import org.bitbucket.takahawk.sortlab.sort.Sorter;

/**
 * Created by takahawk on 19.03.16.
 */
public class SortScene implements Scene {
    private static final String STRINGS_PATH = "i18n/strings";
    private SortLab context;
    private Stage stage;
    private I18NBundle strings;
    private Sorter sorter;
    private SortTask sortTask;
    private SortActor sortActor;
    private VisCheckBox slowMo;
    LabelObserver labelObserver;

    private class LabelObserver extends VisLabel {
        SortTask observable;

        public LabelObserver() {
        }

        @Override
        public void act(float delta) {
            super.act(delta);
            setText("permutations: " + observable.getPermutations() + "\n" +
                    "compares: " + observable.getCompares() + "\n" +
                    "time: " + observable.getTime() / 1000000 + "ms");
        }

        public void setObservable(SortTask sortTask) {
            observable = sortTask;
        }
    }


    public SortScene(Viewport viewport, SortLab context) {
        this.context = context;
        stage = new Stage(viewport);
        AssetManager assetManager = context.getAssetManager();
        if (!assetManager.isLoaded(STRINGS_PATH)) {
            assetManager.load(STRINGS_PATH, I18NBundle.class);
            assetManager.finishLoading();
        }
        strings = assetManager.get(STRINGS_PATH);
        initUI();

        sorter = new BubbleSorter();
    }

    public Stage getStage() {
        return stage;
    }

    public void render(float dt) {
        stage.act(dt);
        stage.draw();
    }


    public void setArray(int[] array) {
        sortTask = new SortTask(array, sorter);
        sortActor.setArray(sortTask.getArray());
        int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;
        for (int a : array) {
            if (max < a) max = a;
            if (min > a) min = a;
        }
        sortActor.setMinValue(min);
        sortActor.setMaxValue(max);
        labelObserver.setObservable(sortTask);
        slowMo.setChecked(false);
    }

    public void setSorter(Sorter sorter) {
        sortTask.setSorter(sorter);
    }

    public void initUI() {
        if (!VisUI.isLoaded())
            VisUI.load();
        Table table = new VisTable();
        table.setFillParent(true);
        table.top();

        sortActor = new SortActor(null, 0, 0);
        sortActor.setWidth(320);
        sortActor.setHeight(400);
        table.add(sortActor).pad(20).colspan(3);
        table.row();

        labelObserver = new LabelObserver();
        table.add(labelObserver).colspan(2);
        slowMo = new VisCheckBox(strings.get("slowMo"));
        table.add(slowMo);
        slowMo.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                System.out.println("Slow: " + slowMo.isChecked());
                sortTask.setSlow(slowMo.isChecked());
            }
        });
        table.row();

        TextButton closeButton = new VisTextButton(strings.get("close"));
        closeButton.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                context.setMainScene();
            }
        });
        table.add(closeButton);
        TextButton sortButton = new VisTextButton(strings.get("sort"));
        sortButton.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                new Thread(sortTask).start();
            }
        });
        table.add(sortButton);
        TextButton resetButton = new VisTextButton(strings.get("reset"));
        resetButton.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                sortTask.reset();
            }
        });
        table.add(resetButton);
        table.row();
        stage.addActor(table);
    }

    public void dispose() {
        if (VisUI.isLoaded())
            VisUI.dispose();
    }
}
