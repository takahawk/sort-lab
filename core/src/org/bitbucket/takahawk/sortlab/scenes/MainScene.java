package org.bitbucket.takahawk.sortlab.scenes;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.building.CenteredTableBuilder;
import com.kotcrab.vis.ui.widget.*;
import org.bitbucket.takahawk.sortlab.SortLab;
import org.bitbucket.takahawk.sortlab.actors.SortActor;


/**
 * Created by takahawk on 19.03.16.
 */
public final class MainScene implements Scene {
    private final Stage stage;
    private final I18NBundle strings;
    private final AssetManager assetManager;
    private final SortLab context;
    VisTextField fileNameField;
    List languages, algorithms;

    public MainScene(Viewport viewport, SortLab context) {
        this.context = context;
        VisUI.load();
        stage = new Stage(viewport);
        assetManager = context.getAssetManager();
        assetManager.load("i18n/strings", I18NBundle.class);
        assetManager.finishLoading();
        strings = assetManager.get("i18n/strings");
        initUI();
    }

    private Label label(String text, float fontScale) {
        Label label = new VisLabel(text);
        label.setFontScale(fontScale);
        return label;
    }


    private void initUI() {
        VisTable table = new VisTable();
        table.setFillParent(true);
        table.top();
        table.left();

        table.add(label(strings.get("sortAlgorithmsLabel"), 2)).align(Align.left).pad(10); table.row();
        table.add(label(strings.get("chooseLanguageLabel"), 1.5f)).pad(10).align(Align.left); table.row();

        languages = new VisList();
        languages.setItems(new Array<String>(
                new String[] {
                        "Java",
                        "C++"
                }
        ));
        table.add(languages).pad(10).padLeft(50).align(Align.left); table.row();

        table.add(label(strings.get("chooseAlgorithmLabel"), 1.5f)).pad(10).align(Align.left).padTop(50); table.row();
        algorithms = new VisList();
        algorithms.setItems(new Array<String>(
                new String[] {
                        strings.get("bubbleSort"),
                        strings.get("insertionSort"),
                        strings.get("shellSort"),
                        strings.get("quickSort"),
                        strings.get("mergeSort")
                }
        ));
        table.add(algorithms).padLeft(50).align(Align.left); table.row();


        Button chooseFileButton = new VisTextButton(strings.get("chooseFile"));
        fileNameField = new VisTextField();
        chooseFileButton.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                context.setFileScene();
            }
        });
        Table fileTable = new VisTable();
        fileTable.add(chooseFileButton).pad(10).padRight(20);
        fileTable.add(fileNameField);
        fileTable.row();
        table.add(fileTable).padTop(50);
        table.row();

        TextButton startButton = new VisTextButton(strings.get("run"));
        startButton.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                context.setSortScene();
            }
        });
        table.add(startButton).padTop(50).center();
        table.row();

        stage.addActor(table);
    }

    public int getAlgorithmIndex() {
        return algorithms.getSelectedIndex();
    }

    public int getLanguageIndex() {
        return languages.getSelectedIndex();
    }

    public void setFilename(String filename) {
        fileNameField.setText(filename);
    }

    public Stage getStage() {
        return stage;
    }

    public void render(float dt) {
         stage.draw();
    }

    public void dispose() {
        VisUI.dispose();
    }
}
