package org.bitbucket.takahawk.sortlab.sort;

/**
 * Created by takahawk on 19.03.16.
 */
public interface Sorter {
    void sort(int[] array, SortStats sortStats, boolean slow);
}
