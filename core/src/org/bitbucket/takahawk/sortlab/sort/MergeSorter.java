package org.bitbucket.takahawk.sortlab.sort;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Created by takahawk on 19.03.16.
 */
public class MergeSorter implements Sorter {

    @Override
    public void sort(int[] array, SortStats sortStats, boolean slow) {
        recur(array, 0, array.length, sortStats, slow);
    }

    public void recur(int[] array, int l, int r, SortStats sortStats, boolean slow) {
        if (r - l <= 1)
            return;
        int mid = l + (r - l) / 2;
        recur(array, l, mid, sortStats, slow);
        recur(array, mid, r, sortStats, slow);
        merge(array, sortStats, l, mid, r, slow);
    }

    public void merge(int[] array, SortStats sortStats, int l, int m, int r, boolean slow) {
        if (slow) {
            try {
                TimeUnit.NANOSECONDS.sleep(100);
            } catch(InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        int[] temp = Arrays.copyOfRange(array, l, r);
        int k = l;
        int i = 0, j = m - l;
        sortStats.compares++;
        while(i < m - l && j < temp.length) {
            sortStats.compares++;
            if (temp[i] < temp[j])
                array[k++] = temp[i++];
            else
                array[k++] = temp[j++];
            sortStats.permutations++;
        }
        sortStats.compares++;
        while (i < m - l) {
            sortStats.compares++;
            array[k++] = temp[i++];
            sortStats.permutations++;
        }
        sortStats.compares++;
        while (j < temp.length) {
            array[k++] = temp[j++];
            sortStats.permutations++;
        }
    }
}
