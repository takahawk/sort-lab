package org.bitbucket.takahawk.sortlab.sort;

import java.util.concurrent.TimeUnit;

/**
 * Created by takahawk on 19.03.16.
 */
public class InsertionSorter implements Sorter {

    @Override
    public void sort(int[] array, SortStats sortStats, boolean slow) {
        for (int i = 1; i < array.length; i++) {
            if (slow) {
                try {
                    TimeUnit.NANOSECONDS.sleep(1);
                } catch(InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            int x = array[i];
            int j = i - 1;
            sortStats.compares++;
            while (j >= 0 && array[j] > array[j + 1]) {
                sortStats.compares++;
                sortStats.permutations++;
                int temp = array[j + 1];
                array[j + 1] = array[j];
                array[j] = temp;
                j--;
            }
        }
    }
}
