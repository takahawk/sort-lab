package org.bitbucket.takahawk.sortlab.sort;

import java.util.concurrent.TimeUnit;

/**
 * Created by takahawk on 19.03.16.
 */
public class QuickSorter implements Sorter {;

    @Override
    public void sort(int[] array, SortStats sortStats, boolean slow) {
        recur(array, sortStats, 0, array.length - 1, slow);
    }


    private void recur(int[] array, SortStats sortStats, int l, int r, boolean slow) {
        if (slow) {
            try {
                TimeUnit.NANOSECONDS.sleep(1);
            } catch(InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        int i = l;
        int j = r;
        int pivot = array[l + (r - l) / 2];
        while (i < j) {
            sortStats.compares++;
            while(array[i] < pivot) {
                sortStats.compares++;
                i++;
            }
            sortStats.compares++;
            while(array[j] > pivot) {
                sortStats.compares++;
                j--;
            }
            if (i <= j) {
                sortStats.permutations++;
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (i < r) recur(array, sortStats, i, r, slow);
        if (l < j) recur(array, sortStats, l, j, slow);
    }
}
