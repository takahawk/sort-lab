package org.bitbucket.takahawk.sortlab.sort;

import java.util.concurrent.TimeUnit;

/**
 * Created by takahawk on 19.03.16.
 */
public class BubbleSorter implements Sorter {

    @Override
    public void sort(int[] array, SortStats sortStats, boolean slow) {
        for (int i = array.length - 1; i > 0; i--) {
            if (slow) {
                try {
                    TimeUnit.NANOSECONDS.sleep(1);
                } catch(InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            for (int j = 0; j < array.length - 1; j++) {
                sortStats.compares++;
                if (array[j] > array[j + 1]) {
                    sortStats.permutations++;
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

}
