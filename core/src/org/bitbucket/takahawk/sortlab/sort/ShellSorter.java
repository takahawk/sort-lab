package org.bitbucket.takahawk.sortlab.sort;

import java.util.concurrent.TimeUnit;

/**
 * Created by takahawk on 19.03.16.
 */
public class ShellSorter implements Sorter {
    @Override
    public void sort(int[] array, SortStats sortStats, boolean slow) {
        for (int step = array.length / 2; step > 0; step /= 2) {
            for (int i = step; i < array.length; i += step) {
                if (slow) {
                    try {
                        TimeUnit.NANOSECONDS.sleep(1);
                    } catch(InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                int x = array[i];
                int j = i - step;
                sortStats.compares++;
                while (j >= 0 && array[j + step] < array[j]) {
                    sortStats.compares++;
                    sortStats.permutations++;
                    int temp = array[j + step];
                    array[j + step] = array[j];
                    array[j] = temp;
                    j -= step;
                }
            }
        }
    }
}
