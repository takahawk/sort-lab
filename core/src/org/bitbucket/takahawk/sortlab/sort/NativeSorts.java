package org.bitbucket.takahawk.sortlab.sort;

/**
 * Created by takahawk on 20.03.16.
 */
public class NativeSorts {

    static {
        // System.setProperty("java.library.path", System.getProperty("user.dir") + "/lib");
        System.out.println(System.getProperty("java.library.path"));
        System.loadLibrary("NativeSorts");
    }
    public native void bubbleSort(int[] array, SortStats stats, boolean slow);
    public native void insertionSort(int[] array, SortStats stats, boolean slow);
    public native void shellSort(int[] array, SortStats stats, boolean slow);
    public native void quickSort(int[] array, SortStats stats, boolean slow);
    public native void mergeSort(int[] array, SortStats stats, boolean slow);
}
