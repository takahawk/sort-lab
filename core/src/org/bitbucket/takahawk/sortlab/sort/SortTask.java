package org.bitbucket.takahawk.sortlab.sort;

import java.util.Arrays;

/**
 * Created by takahawk on 19.03.16.
 */
public class SortTask implements Runnable {
    private SortStats sortStats = new SortStats();
    private long initTime;
    private long totalTime;
    private Sorter sorter;
    private int[] array;
    private int[] initArray;
    private boolean finished = true;
    private boolean slow = false;

    public SortTask(int[] array, Sorter sorter) {
        initArray = array;
        this.array = Arrays.copyOf(initArray, initArray.length);
        this.sorter = sorter;
    }

    public long getPermutations() {
        return sortStats.permutations;
    }
    public long getCompares() {
        return sortStats.compares;
    }
    public long getTime() {
        if (finished)
            return totalTime;
        else
            return System.nanoTime() - initTime;
    }

    public int[] getArray() {
        return array;
    }

    public void setSorter(Sorter sorter) {
        this.sorter = sorter;
    }

    public void reset() {
        if (finished) {
            for (int i = 0; i < initArray.length; i++) {
                array[i] = initArray[i];
            }
            sortStats.compares = 0;
            sortStats.permutations = 0;
            totalTime = 0;
        }
    }

    public void setSlow(boolean slow) {
        this.slow = slow;
    }
    @Override
    public void run() {
        finished = false;
        initTime = System.nanoTime();
        sorter.sort(array, sortStats, slow);
        totalTime = System.nanoTime() - initTime;
        finished = true;
    }
}
