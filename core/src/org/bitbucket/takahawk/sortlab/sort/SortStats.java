package org.bitbucket.takahawk.sortlab.sort;

/**
 * Created by takahawk on 19.03.16.
 */
public class SortStats {
    public long permutations;
    public long compares;
}
