package org.bitbucket.takahawk.sortlab.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by takahawk on 19.03.16.
 */
public class SortActor extends Actor {
    private static final int RADIUS = 1;
    private int[] observableArray;
    private ShapeRenderer shapeRenderer = new ShapeRenderer();
    private int maxValue, minValue;


    public SortActor(int[] observableArray, int maxValue, int minValue) {
        this.observableArray = observableArray;
        this.maxValue = maxValue;
        this.minValue = minValue;
    }

    public void setArray(int[] array) {
        this.observableArray = array;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.end();
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        drawDebug(shapeRenderer);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(getX(), getY(), getWidth(), getHeight());
        shapeRenderer.setColor(Color.WHITE);
        float stepX = getWidth() / observableArray.length;
        float valueY = getHeight() / (maxValue - minValue);
        for (int i = 0; i < observableArray.length; i++) {
            shapeRenderer.circle(getX() + i * stepX, minValue + getY() + valueY * observableArray[i], RADIUS);
        }
        shapeRenderer.end();
        batch.begin();
    }

    @Override
    public boolean remove() {
        shapeRenderer.dispose();
        return super.remove();
    }
}
