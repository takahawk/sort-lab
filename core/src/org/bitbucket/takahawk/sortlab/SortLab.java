package org.bitbucket.takahawk.sortlab;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import org.bitbucket.takahawk.sortlab.scenes.FileScene;
import org.bitbucket.takahawk.sortlab.scenes.MainScene;
import org.bitbucket.takahawk.sortlab.scenes.Scene;
import org.bitbucket.takahawk.sortlab.scenes.SortScene;
import org.bitbucket.takahawk.sortlab.sort.*;
import org.bitbucket.takahawk.sortlab.util.ArrayGenerator;
import org.bitbucket.takahawk.sortlab.util.ArrayLoader;

import java.io.File;
import java.io.FileNotFoundException;

public final class SortLab extends ApplicationAdapter {
	public static final int WIDTH = 360;
	public static final int HEIGHT = 640;
	private Scene currentScene;
	private MainScene mainWindow;
	private FileScene fileScene;
	private SortScene sortScene;
    private ArrayLoader arrayLoader = new ArrayLoader();
	private AssetManager assetManager = new AssetManager();

	@Override
	public void create () {
        Viewport viewport = new FitViewport(WIDTH, HEIGHT);
		assetManager.load("i18n/strings", I18NBundle.class);
		assetManager.finishLoading();
		mainWindow = new MainScene(viewport, this);
		fileScene = new FileScene(viewport, this);
		sortScene = new SortScene(viewport, this);
		currentScene = mainWindow;
		Gdx.input.setInputProcessor(mainWindow.getStage());
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		currentScene.render(Gdx.graphics.getDeltaTime());
	}

	public void setFileScene() {
		currentScene = fileScene;
		Gdx.input.setInputProcessor(fileScene.getStage());
	}

	public void setMainScene() {
		currentScene = mainWindow;
		if (fileScene.getLastFile() != null)
			mainWindow.setFilename(fileScene.getLastFile().path());
		Gdx.input.setInputProcessor(mainWindow.getStage());
	}

	public void setSortScene() {
		currentScene = sortScene;
        try {
            sortScene.setArray(arrayLoader.fromFile(fileScene.getLastFile().file()));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
		if (mainWindow.getLanguageIndex() == 1) {
			System.out.println("Native sorts");
            switch (mainWindow.getAlgorithmIndex()) {
                case 0:
                    sortScene.setSorter(new Sorter() {
                        NativeSorts sorts = new NativeSorts();
                        @Override
                        public void sort(int[] array, SortStats sortStats, boolean slow) {
                            sorts.bubbleSort(array, sortStats, slow);
                        }
                    });
                    break;
                case 1:
                    sortScene.setSorter(new Sorter() {
                        NativeSorts sorts = new NativeSorts();
                        @Override
                        public void sort(int[] array, SortStats sortStats, boolean slow) {
                            sorts.insertionSort(array, sortStats, slow);
                        }
                    });
                    break;
                case 2:
                    sortScene.setSorter(new Sorter() {
                        NativeSorts sorts = new NativeSorts();
                        @Override
                        public void sort(int[] array, SortStats sortStats, boolean slow) {
                            sorts.shellSort(array, sortStats, slow);
                        }
                    });
                    break;
                case 3:
                    sortScene.setSorter(new Sorter() {
                        NativeSorts sorts = new NativeSorts();
                        @Override
                        public void sort(int[] array, SortStats sortStats, boolean slow) {
                            sorts.quickSort(array, sortStats, slow);
                        }
                    });
                    break;
            }

		} else
			switch (mainWindow.getAlgorithmIndex()) {
				case 0:
					System.out.println("bubble sorter");
					sortScene.setSorter(new BubbleSorter());
					break;
				case 1:
					System.out.println("insertion sorter");
					sortScene.setSorter(new InsertionSorter());
					break;
				case 2:
					System.out.println("shell sorter");
					sortScene.setSorter(new ShellSorter());
					break;
				case 3:
					System.out.println("quick sorter");
					sortScene.setSorter(new QuickSorter());
					break;
				case 4:
					System.out.println("merge sorter");
					sortScene.setSorter(new MergeSorter());
				default:
			}
		Gdx.input.setInputProcessor(sortScene.getStage());
	}

	public MainScene getMainScene() {
		return mainWindow;
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}

	@Override
	public void dispose() {
		super.dispose();
		mainWindow.dispose();
		assetManager.dispose();
	}
}
