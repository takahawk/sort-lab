package org.bitbucket.takahawk.sortlab.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.bitbucket.takahawk.sortlab.SortLab;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = SortLab.WIDTH;
		config.height = SortLab.HEIGHT;
		config.resizable = false;
		new LwjglApplication(new SortLab(), config);
	}
}
