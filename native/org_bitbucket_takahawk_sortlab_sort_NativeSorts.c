#include <jni.h>
#include <stdio.h>
#include "org_bitbucket_takahawk_sortlab_sort_NativeSorts.h"

JNIEXPORT void JNICALL Java_org_bitbucket_takahawk_sortlab_sort_NativeSorts_bubbleSort
  (JNIEnv * env, jobject thisObj, jintArray arr, jobject sortStats, jboolean slow) {
    int i = 0, j = 0;
    jsize len = (*env)->GetArrayLength(env, arr);
    jint *body = (*env)->GetPrimitiveArrayCritical(env, arr, 0);
    jint temp;
    for (i = len - 1; i > 0; i--) {
        for (j = 0; j < i; j++) {
            if (body[j] > body[j + 1]) {
                temp = body[j];
                body[j] = body[j + 1];
                body[j + 1] = temp;
            }
        }
    }
    (*env)->ReleasePrimitiveArrayCritical(env, arr, body, 0);
  }

JNIEXPORT void JNICALL Java_org_bitbucket_takahawk_sortlab_sort_NativeSorts_insertionSort
  (JNIEnv * env, jobject thisObj, jintArray arr, jobject sortStats, jboolean slow) {
      int i, j;
      jsize len = (*env)->GetArrayLength(env, arr);
      jint *body = (*env)->GetPrimitiveArrayCritical(env, arr, 0);
      jint temp;
      for (i = 1; i < len; i++) {
        j = i;
        while (j > 0 && body[j - 1] > body[j]) {
            temp = body[j - 1];
            body[j - 1] = body[j];
            body[j] = temp;
            j--;
        }
      }
      (*env)->ReleasePrimitiveArrayCritical(env, arr, body, 0);
  }

JNIEXPORT void JNICALL Java_org_bitbucket_takahawk_sortlab_sort_NativeSorts_shellSort
  (JNIEnv * env, jobject thisObj, jintArray arr, jobject sortStats, jboolean slow) {
    int gap, i, j;
    jsize len = (*env)->GetArrayLength(env, arr);
    jint *body = (*env)->GetPrimitiveArrayCritical(env, arr, 0);
    jint temp;
    for (gap = len / 2; gap > 0; gap /= 2) {
      for (i = gap; i < len; i += gap) {
        j = i;
        while (j > 0 && body[j - gap] > body[j]) {
            temp = body[j - gap];
            body[j - gap] = body[j];
            body[j] = temp;
            j -= gap;
        }
      }
    }
    (*env)->ReleasePrimitiveArrayCritical(env, arr, body, 0);
  }

    void recurQuick(jint* array, int l, int r) {
        int i = l, j = r;
        int pivot = array[l + (r - l) / 2];
        int temp;
        while (i <= j) {
          while(array[i] < pivot) i++;
          while(array[j] > pivot) j--;
          if (i <= j) {
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            i++;
            j--;
          }
        }
        if (i < r) recurQuick(array, i, r);
        if (l < j) recurQuick(array, l, j);
      }

  JNIEXPORT void JNICALL Java_org_bitbucket_takahawk_sortlab_sort_NativeSorts_quickSort
    (JNIEnv * env, jobject thisObj, jintArray arr, jobject sortStats, jboolean slow) {
      jsize len = (*env)->GetArrayLength(env, arr);
      jint *body = (*env)->GetPrimitiveArrayCritical(env, arr, 0);
      jint temp;
      recurQuick(body, 0, len - 1);
    }

